terraform {
  backend "s3" {
    bucket = "terraform-aws-microservices-demo"
    key = "microservices-terraform-vpc.state"
    region = "ap-southeast-1"
  }

  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }
}

resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"

  tags =  {
    Name = "Prod VPC Microservices"
  }
}

resource "aws_subnet" "public-subnet1" {
  vpc_id = aws_vpc.main.id
  cidr_block = "10.0.1.0/24"
  map_public_ip_on_launch = true
  availability_zone = "ap-southeast-1a"

  tags = {
    Name = "public-subnet1"
  }
}

resource "aws_subnet" "public-subnet2" {
  vpc_id = aws_vpc.main.id
  cidr_block = "10.0.2.0/24"
  map_public_ip_on_launch = true
  availability_zone = "ap-southeast-1b"

  tags = {
    Name = "public-subnet2"
  }
}

resource "aws_subnet" "private-subnet1" {
  vpc_id = aws_vpc.main.id
  cidr_block = "10.0.3.0/24"
  map_public_ip_on_launch = false
  availability_zone = "ap-southeast-1a"

  tags = {
    Name = "private-subnet1"
  }
}

resource "aws_subnet" "private-subnet2" {
  vpc_id = aws_vpc.main.id
  cidr_block = "10.0.4.0/24"
  map_public_ip_on_launch = false
  availability_zone = "ap-southeast-1b"

  tags = {
    Name = "private-subnet2"
  }
}

resource "aws_internet_gateway" "microservices-demo-gw" {
  vpc_id = aws_vpc.main.id
}

resource "aws_route_table" "microservices-demo-rt" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.microservices-demo-gw.id
  }
}

resource "aws_route_table_association" "subnet1-rt" {
  subnet_id = aws_subnet.public-subnet1.id
  route_table_id = aws_route_table.microservices-demo-rt.id
}

resource "aws_route_table_association" "subnet2-rt" {
  subnet_id = aws_subnet.public-subnet2.id
  route_table_id = aws_route_table.microservices-demo-rt.id
}

resource "aws_eip" "nat_gateway" {
  domain = "vpc"
  depends_on = [ aws_internet_gateway.microservices-demo-gw ]
}