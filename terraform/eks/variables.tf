variable eksIAMRole {
  type = string
  default = "microservices-demo-iam-role"
}
variable EKSClusterName {
  type = string
  default = "microservices-demo-prod-eks-cluster"
}
variable public-subnet1 {
  type = string
  default = "subnet-078aaaed74b6a2a03"
}
variable public-subnet2 {
  type = string
  default = "subnet-045c9372777ef8fce"
}
variable private-subnet1 {
  type = string
  default = "subnet-04572c43a6422cb69"
}
variable private-subnet2 {
  type = string
  default = "subnet-04e3b145cc067d40f"
}
variable "k8sVersion" {
  type = string
  default = "1.28"
}
variable workerNodeIAM {
  type  = string
  default = "microservices-demo-prod-workernodes"
}
variable instanceType {
  type = list
  default = ["t2.large"]
}
variable environment {
  type = string
  default = "prod"
}
variable desired_size {
  type = string
  default = 3
}
variable max_size {
  type = string
  default = 4
}
variable min_size {
  type = string
  default = 3
}